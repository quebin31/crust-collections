#[macro_export]
macro_rules! mem {
    (new $val:expr) => ( { Box::into_raw(Box::new($val)) as *mut _ }; );
    (del $var:expr) => ( unsafe { drop(Box::from_raw($var)); } );
    (box $var:expr) => ( unsafe { Box::from_raw($var) } );
    (raw $var:expr) => ( { Box::into_raw($var) as *mut _ }; );
}
