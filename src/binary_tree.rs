use std::ptr::null_mut;
use std::{
    error,
    fmt::{self, Display, Debug, Formatter}
};

#[derive(Clone, Debug)]
pub enum ErrorKind {
    DuplicateValue,
    NotFoundValue
}

#[derive(Clone, Debug)]
pub struct Error {
    kind: ErrorKind,
    message: String
}

impl Display for self::Error {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl error::Error for self::Error {
    fn description(&self) -> &str {
        match self.kind {
            ErrorKind::DuplicateValue => "Trying to add a duplicate value",
            ErrorKind::NotFoundValue  => "The element searched isn't in the tree"
        }
    }
}

struct Node<T> {
    data: T,
    left: *mut Node<T>,
    right: *mut Node<T>
}

impl<T> Node<T> {
    fn new(data: T) -> Self {
        Self {
            data,
            left: null_mut(),
            right: null_mut()
        }
    }

    fn left(&self) -> Option<*mut Self> {
        if self.left.is_null() {
            None
        } else {
            Some(self.left)
        }
    }
    
    fn right(&self) -> Option<*mut Self> {
        if self.right.is_null() {
            None
        } else {
            Some(self.right)
        }
    }

    fn is_leaf(&self) -> bool {
        match (self.left(), self.right()) {
            (None, None) => true,
            _            => false
        }
    }
}

impl<T: Debug> Debug for Node<T> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let left = match self.left() {
            None    => "None".to_owned(),
            Some(_) => "Some".to_owned()
        };

        let right = match self.right() {
            None    => "None".to_owned(),
            Some(_) => "Some".to_owned()
        };

        write!(f, "{{ data: {:?}, left: {}, right: {} }}", self.data, left, right)
    }
}

pub struct BinaryTree<T> {
    root: *mut Node<T>,
    size: usize
}

impl<T: PartialOrd + Debug> BinaryTree<T> {
    pub fn new() -> Self {
        Self {
            root: null_mut(),
            size: 0
        }
    }

    pub fn insert(&mut self, data: T) -> Result<(), self::Error> {
        if self.root.is_null() {
            self.root = mem!(new Node::new(data));    
            self.size += 1;
            return Ok(());
        } 
            
        let mut tmp = self.root;
        loop {
            let mut boxed_tmp = mem!(box tmp);
            
            if boxed_tmp.data == data {
                return Err(self::Error { 
                    kind: ErrorKind::DuplicateValue, 
                    message: "Trying to add a duplicate".to_owned()
                });
            }

            if boxed_tmp.data > data {
                match boxed_tmp.left() {
                    None => {
                        boxed_tmp.left = mem!(new Node::new(data));
                        let _ = mem!(raw boxed_tmp);
                        break;
                    },
                    Some(left) => {
                        tmp = left;
                        let _ = mem!(raw boxed_tmp);
                    }
                }
            } else {
                match boxed_tmp.right() {
                    None => {
                        boxed_tmp.right = mem!(new Node::new(data));
                        let _ = mem!(raw boxed_tmp);
                        break;
                    },
                    Some(right) => {
                        tmp = right;
                        let _ = mem!(raw boxed_tmp);
                    }
                }
            }
        }

        self.size += 1;
        Ok(())
    }

    pub fn remove(&mut self, data: T) -> Result<(), self::Error> {
        if self.root.is_null() {
            return Err(self::Error {
                kind: ErrorKind::NotFoundValue,
                message: format!("The element {:?} isn't in the tree", data)
            });
        }

        // TODO: Implement

        Ok(())
    }
}

impl<T: Debug> Debug for BinaryTree<T> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        use queue::Queue;

        let mut queue = Queue::new();
        queue.push(self.root);

        write!(f, "{{")?;

        loop {
            if let Some(node) = queue.pop() {
                let boxed = mem!(box node);
                
                write!(f, " {:?} ", *boxed)?;

                if let Some(left) = boxed.left() {
                    queue.push(left);
                }

                if let Some(right) = boxed.right() {
                    queue.push(right);
                }
            } else { break; }
        }

        write!(f, "}}")
    }
}
